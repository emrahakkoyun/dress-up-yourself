package com.gojolo.dressupyourself;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

public class MainActivity extends Activity {
int screenWidth,screenHeight;
int t=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenWidth =displaymetrics.widthPixels;
		screenHeight=displaymetrics.heightPixels;
		final RelativeLayout rlt = (RelativeLayout) findViewById(R.id.rlt);
		rlt.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				t=0;
				return false;
			}
		});
	}
	 public boolean onTouchEvent(MotionEvent event) {
		 if(screenWidth/event.getX()<3.10&&screenWidth/event.getX()>1.48&&screenHeight/event.getY()<1.30&&screenHeight/event.getY()>1.03)
		 {
			 if(t==0)
			 {Intent image = new Intent(MainActivity.this,Getimage.class);
			 startActivity(image);
			 }
			 t=1;
		 }
		return true;
		}
}
