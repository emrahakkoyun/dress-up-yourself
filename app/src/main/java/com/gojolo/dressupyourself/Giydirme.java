package com.gojolo.dressupyourself;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Giydirme extends Activity {
Bitmap cbitmap;
private static final int CAMERA_REQUEST = 1888; 
int screenWidth,screenHeight;
ImageView g1,g2,g3,g4,g5;
float nx1,ny1,nx2,ny2,nx3,ny3,nx4,ny4,nx5,ny5;
boolean  move1,move2,move3,move4,move5;
int doubleclick1=0,doubleclick2=0,doubleclick3=0,doubleclick4=0,doubleclick5=0;
double i1=2,tt1=0,i2=2,tt2=0,i3=2,tt3=0,i4=2,tt4=0,tt5=0;
double i5=2;
int p=1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_giydirme);
		final RelativeLayout rlt = (RelativeLayout) findViewById(R.id.rlt);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenWidth = displaymetrics.widthPixels;
		screenHeight =displaymetrics.heightPixels;
		rlt.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				p=1;
				return false;
			}
		});
		ImageView img1 =new ImageView(this);
		ImageView img2=new ImageView(this);
		g1 = new  ImageView(this);
	    g2 = new  ImageView(this);
	    g3 = new  ImageView(this);
		g4 = new  ImageView(this);
		g5 = new  ImageView(this);
		Bundle extras = getIntent().getExtras();
	    cbitmap= extras.getParcelable("photo");
	    Bitmap newbitmap =Bitmap.createScaledBitmap(cbitmap,screenWidth,(int)(screenHeight/1.2), true);
	    img1.setImageBitmap(newbitmap);
	    RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(screenWidth,(int)(screenHeight/1.2));
	    RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(screenWidth,(int)(screenHeight-(screenHeight/1.2)));
	    RelativeLayout.LayoutParams g1params = new RelativeLayout.LayoutParams((int)(screenWidth/1.5),(int)(screenHeight/1.5));
	    RelativeLayout.LayoutParams g2params = new RelativeLayout.LayoutParams((int)(screenWidth/1.5),(int)(screenHeight/1.5));
	    RelativeLayout.LayoutParams g3params = new RelativeLayout.LayoutParams((int)(screenWidth/1.5),(int)(screenHeight/1.5));
	    RelativeLayout.LayoutParams g4params = new RelativeLayout.LayoutParams((int)(screenWidth/1.5),(int)(screenHeight/1.5));
	    RelativeLayout.LayoutParams g5params = new RelativeLayout.LayoutParams((int)(screenWidth/1.5),(int)(screenHeight/1.5));
	    g1.setLayoutParams(g1params);
	    g2.setLayoutParams(g2params);
	    g3.setLayoutParams(g3params);
	    g4.setLayoutParams(g4params);
	    g5.setLayoutParams(g5params);
        img1.setLayoutParams(params1);
        img2.setLayoutParams(params2);
        img2.setImageResource(R.drawable.footer);
        img2.setY((int)(screenHeight/1.2));
        rlt.addView(img1);
        rlt.addView(img2);
        rlt.addView(g1);
        rlt.addView(g2);
        rlt.addView(g3);
        rlt.addView(g4);
        rlt.addView(g5);
      g1.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(move1==false)
				{
				doubleclick1++;
				if(doubleclick1==2)
				{
					
					if(i1<9&&tt1==0)
					{ i1+=0.5; }
					else 
					{   tt1=1;
						i1-=0.5;
						if(i1==2)
						{
							tt1=0;
						}
					}
					 g1.getLayoutParams().height =(int)(screenHeight/i1);
					 g1.getLayoutParams().width =(int)(screenWidth/i1);
		        	 g1.requestLayout();
		        	doubleclick1=0;
				}
			}
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move1=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move1=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move1)
			        	{
			        		
			        		nx1=event.getRawX()-g1.getWidth()/2;
			        		ny1=event.getRawY()-g1.getHeight()/2;
			        		v.setX(nx1);
			        		v.setY(ny1);
				        }
			        	break;
			        }
				}
				return true;
			}
		});
      g2.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(move2==false)
				{
				doubleclick2++;
				if(doubleclick2==2)
				{
					
					if(i2<9&&tt2==0)
					{ i2+=0.5; }
					else 
					{   tt2=1;
						i2-=0.5;
						if(i2==2)
						{
							tt2=0;
						}
					}
					 g2.getLayoutParams().height =(int)(screenHeight/i2);
					 g2.getLayoutParams().width =(int)(screenWidth/i2);
		        	 g2.requestLayout();
		        	doubleclick2=0;
				}
			}
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move2=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move2=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move2)
			        	{
			        		
			        		nx2=event.getRawX()-g2.getWidth()/2;
			        		ny2=event.getRawY()-g2.getHeight()/2;
			        		v.setX(nx2);
			        		v.setY(ny2);
				        }
			        	break;
			        }
				}
				return true;
			}
		});
      g3.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(move3==false)
				{
				doubleclick3++;
				if(doubleclick3==2)
				{
					
					if(i3<9&&tt2==0)
					{ i3+=0.5; }
					else 
					{   tt3=1;
						i3-=0.5;
						if(i3==2)
						{
							tt3=0;
						}
					}
					 g3.getLayoutParams().height =(int)(screenHeight/i3);
					 g3.getLayoutParams().width =(int)(screenWidth/i3);
		        	 g3.requestLayout();
		        	doubleclick3=0;
				}
			}
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move3=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move3=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move3)
			        	{
			        		
			        		nx3=event.getRawX()-g3.getWidth()/2;
			        		ny3=event.getRawY()-g3.getHeight()/2;
			        		v.setX(nx3);
			        		v.setY(ny3);
				        }
			        	break;
			        }
				}
				return true;
			}
		});
      g4.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(move4==false)
				{
				doubleclick4++;
				if(doubleclick4==2)
				{
					
					if(i4<9&&tt4==0)
					{ i4+=0.5;}
					else 
					{   tt4=1;
						i4-=0.5;
						if(i4==2)
						{
							tt4=0;
						}
					}
					 g4.getLayoutParams().height =(int)(screenHeight/i4);
					 g4.getLayoutParams().width =(int)(screenWidth/i4);
		        	 g4.requestLayout();
		        	doubleclick4=0;
				}
			}
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move4=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move4=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move4)
			        	{
			        		
			        		nx4=event.getRawX()-g4.getWidth()/2;
			        		ny4=event.getRawY()-g4.getHeight()/2;
			        		v.setX(nx4);
			        		v.setY(ny4);
				        }
			        	break;
			        }
				}
				return true;
			}
		});
      g5.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(move5==false)
				{
				doubleclick5++;
				if(doubleclick5==2)
				{
					
					if(i5<9&&tt5==0)
					{ i5=i5+0.5; }
					else 
					{   tt5=1;
						i5=i5-0.5;
						if(i5==1.5)
						{
							tt5=0;
						}
					}
					 g5.getLayoutParams().height =(int)(screenHeight/i5);
					 g5.getLayoutParams().width =(int)(screenWidth/i5);
		        	 g5.requestLayout();
		        	doubleclick5=0;
				}
			}
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move5=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move5=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move5)
			        	{
			        		
			        		nx5=event.getRawX()-g5.getWidth()/2;
			        		ny5=event.getRawY()-g5.getHeight()/2;
			        		v.setX(nx5);
			        		v.setY(ny5);
				        }
			        	break;
			        }
				}
				return true;
			}
		});
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
            Bitmap photo = (Bitmap) data.getExtras().get("data"); 
            if(g1.getDrawable()==null)
            {
                g1.setImageBitmap(photo);
            }
            else{
            	if(g2.getDrawable()==null)
            	{
            		g2.setImageBitmap(photo);
            	}
            	else{
            		if(g3.getDrawable()==null)
                	{
                		g3.setImageBitmap(photo);
                	}
            		else{
            			if(g4.getDrawable()==null)
                    	{
                    		g4.setImageBitmap(photo);
                    	}
            			else{
            				if(g5.getDrawable()==null)
                        	{
                        		g5.setImageBitmap(photo);
                        	}
            			}
            		}
            	}
            }
        }  
    } 
	public boolean onTouchEvent(MotionEvent event) {
		Toast.makeText(getApplication(),"X:"+String.valueOf(screenWidth/event.getX())+"Y:"+String.valueOf(screenHeight/event.getY()),Toast.LENGTH_LONG).show();
		 if(screenWidth/event.getX()<200&&screenWidth/event.getX()>1.01&&screenHeight/event.getY()<1.20&&screenHeight/event.getY()>1.009)
		 {
			 if(p==1)
			 {Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
             startActivityForResult(cameraIntent, CAMERA_REQUEST); 
			 }
			 p=0;
		 }
		return true;

		}
}
