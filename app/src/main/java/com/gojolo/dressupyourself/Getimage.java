package com.gojolo.dressupyourself;

import java.io.FileNotFoundException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class Getimage extends Activity {
int screenWidth,screenHeight;
private static final int CAMERA_REQUEST = 1888; 
private static int RESULT_LOAD_IMG = 1;
int t1=0,t2=0;
    private InterstitialAd mInterstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_getimage);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        MobileAds.initialize(this,
                "ca-app-pub-1312048647642571~4999792245");
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenWidth =displaymetrics.widthPixels;
		screenHeight=displaymetrics.heightPixels;
		final RelativeLayout rlt = (RelativeLayout) findViewById(R.id.rlt);
		rlt.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				t1=0; t2=0;
				return false;
			}
		});
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            Intent  pintent = new Intent(Getimage.this,Giydirme.class);
            pintent.putExtra("photo",photo);
            startActivity(pintent);
        }  
        super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {

                Uri selectedImage = data.getData();
                try {
                   Bitmap photo =decodeBitmap(selectedImage );
                   Intent  pintent = new Intent(Getimage.this,Giydirme.class);
                   pintent.putExtra("photo",photo);
                   startActivity(pintent);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }
	public  Bitmap decodeBitmap(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
    }
	 public boolean onTouchEvent(MotionEvent event) {
		 if(screenWidth/event.getX()<9.24&&screenWidth/event.getX()>2.38&&screenHeight/event.getY()<1.92&&screenHeight/event.getY()>1.51)
		 {
			 if(t1==0)
             {
                 inrequestadd(1);
             }
			 t1=1;
		 }
		 if(screenWidth/event.getX()<1.63&&screenWidth/event.getX()>1.08&&screenHeight/event.getY()<1.92&&screenHeight/event.getY()>1.51)
		 {
			 if(t2==0)
             {         inrequestadd(2);
             }
			 t2=1;
		 }
		return true;

		}
    public boolean inrequestadd(final int gelen) {
        mInterstitial = new InterstitialAd(Getimage.this);
        //ca-app-pub-1312048647642571/5631917786 reklam
        mInterstitial.setAdUnitId("ca-app-pub-1312048647642571/5285293229");
        mInterstitial.loadAd(new AdRequest.Builder().build());
        mInterstitial.setAdListener(new AdListener() {

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                if(gelen==1){

                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
                else {

                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    // Start the Intent
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                }
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                if(gelen==1){

                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
                else {

                    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    // Start the Intent
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                }
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if (mInterstitial.isLoaded()) {
                    mInterstitial.show();
                }
                else {
                    if(gelen==1){

                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                    else {

                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        // Start the Intent
                        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
                    }
                }
            }

        });

        return false;
    }
}
